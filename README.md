# Frontend Boilerplate #
Bộ công cụ lập trình Frontend tối ưu của Techcube JSC tổng hợp.
## Yêu cầu ##
Cần có NodeJS phiên bản mới nhất [Download](https://nodejs.org/download/) 

## Chạy lần lượt các lệnh sau để cài đặt những thành phần cần thiết ##

3 lệnh đầu chỉ chạy trong trường hợp chưa từng dùng nó trước đây
```
npm update -g bower
npm cache clean
bower cache clean
```

2 lệnh sau là bắt buộc
```
npm install
bower install
```



## Sử dụng ##

Chạy lệnh `bower install --save <package>` để cài đặt các gói Bower UI cần thiết [[Tra cứu danh sách]](http://bower.io/search)

Chạy lệnh `gulp serve` để code (code đến đâu nội dung cập nhật đến đó)

Chạy lệnh `gulp` để build ra bản phát hành (nội dung được build ra nằm trong thư mục **dist**)